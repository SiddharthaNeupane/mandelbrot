﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mandelbrot_Project
{

    public class HSB
    {
        public float rChan, gChan, bChan;
        public bool color = false;
        public Char colorValue;
        public float newValue;
        
        public HSB()
        {
            rChan = gChan = bChan = 0;
        }
        public void Color(Char colorValue)
        {
            this.colorValue = colorValue;
            color = true;

        }

        public void colorCycling(int i)
        {
            char[] colors = { 'R', 'G','B','P','D'};

            
                this.colorValue = colors[i];  
        }
        
        public void fromHSB(float h, float s, float b)
        {
            if (s == 0)
            {
                rChan = gChan = bChan = (int)(b * 255.0f + 0.5f);
            }
            else
            {
                h = (h - (float)Math.Floor(h)) * 5.0f;
                float f = h - (float)Math.Floor(h);
                float p = b * (1.0f - s);
                float q = b * (1.0f - s * f);
                float t = b * (1.0f - (s * (1.0f - f)));

                switch ((int)h)
                {

                    case 0:
                        switch (colorValue) {
                            case 'B':
                                rChan = (int)(p * 255.0f + 0.5f);
                                gChan = (int)(t * 255.0f + 0.5f);
                                bChan = (int)(b * 255.0f + 0.5f);
                                break;
                            case 'G':
                                rChan = (int)(p * 255.0f + 0.5f);
                                gChan = (int)(b * 255.0f + 0.5f);
                                bChan = (int)(t * 255.0f + 0.5f);
                                break;
                            case 'P':
                                rChan = (int)(b * 255.0f + 0.5f);
                                gChan = (int)(t * 255.0f + 0.5f);
                                bChan = (int)(b * 255.0f + 0.5f);
                                break;
                            case 'D':
                                rChan = (int)(b * 0.0f + 0.5f);
                                gChan = (int)(t * 0.0f + 0.5f);
                                bChan = (int)(p * 0.0f + 0.5f);
                                break;
                            default:
                                rChan = (int)(b * 255.0f + 0.5f);
                                gChan = (int)(t * 255.0f + 0.5f);
                                bChan = (int)(p * 255.0f + 0.5f);
                                break;

                        }
                        break;
                
                        case 1:
                             rChan = (int)(q * 255.0f + 0.5f);
                             gChan = (int)(b * 255.0f + 0.5f);
                             bChan = (int)(p * 255.0f + 0.5f);
                             break;

                         case 2:
                             rChan = (int)(p * 255.0f + 0.5f);
                             gChan = (int)(b * 255.0f + 0.5f);
                             bChan = (int)(t * 255.0f + 0.5f);
                             break;

                         case 3:
                             rChan = (int)(p * 255.0f + 0.5f);
                             gChan = (int)(q * 255.0f + 0.5f);
                             bChan = (int)(b * 255.0f + 0.5f);
                             break;

                         case 4:
                             rChan = (int)(t * 255.0f + 0.5f);
                             gChan = (int)(p * 255.0f + 0.5f);
                             bChan = (int)(b * 255.0f + 0.5f);
                             break;
                         case 5:
                             rChan = (int)(b * 255.0f + 0.5f);
                             gChan = (int)(p * 255.0f + 0.5f);
                             bChan = (int)(q * 255.0f + 0.5f);
                             break;
                         default:
                             rChan = (int)(b * 0.0f + 0.5f);
                             gChan = (int)(p * 0.0f + 0.5f);
                             bChan = (int)(q * 128.0f + 0.5f);
                             break;

                        }
                 }
            }
    }
        
}
