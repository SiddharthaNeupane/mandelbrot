﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Printing;

namespace Mandelbrot_Project
{
    [Serializable]
    public partial class projectfractal : Form
    {
        private const int MAX = 256;      // max iterations
        private  double SX = -2.025; // start value real
        private  double SY = -1.125; // start value imaginary
        private  double EX = 0.6;    // end value real
        private  double EY = 1.125;  // end value imaginary
        private static int x1, y1, xs, ys, xe, ye;
        private static double xstart, ystart, xende, yende, xzoom, yzoom;
        private static bool action, rectangle;
        private static bool finished = false;
        private static float xy;
        private bool mouseDown = false;
        //private Image picture;
        private Bitmap picture;
        //private Graphics g;
        private Graphics g1;
        private Cursor c1, c2;
        private HSB HSBcol;
        private Pen pen;
        private Rectangle rect;
        private double newSX, newSY, newEX, newEY;
        private bool mandelbrotStateValue = false;
        private int colorCycle;
        private char ColorPalate;
        private char newcolor;
        //private float hue;
        private void startToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
        }

        private void cloneToolStripMenuItem_Click(object sender, EventArgs e)
        {
            projectfractal obj = new projectfractal();
            obj.Show();
        }

        private void tagToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Tag...");
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            e.Graphics.DrawImage(pictureBox1.Image, 0, 0);
        }

        private void printToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PrintDocument p = new PrintDocument();
            p.PrintPage += new PrintPageEventHandler(printDocument1_PrintPage);
            p.Print();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            SaveFileDialog f = new SaveFileDialog();
            f.Filter = "JPG(*.JPG) | *.JPG";
            if (f.ShowDialog()  == DialogResult.OK)
            {
                picture.Save(f.FileName);
            }
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Stop();
        }

        

        private void restartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

      

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            //initvalues();
        
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void saveStateToolStripMenuItem_Click(object sender, EventArgs e)
        {
          
        
        SaveFileDialog f = new SaveFileDialog();
            //f.Filter = "JPG(*.JPG) | *.JPG";
            f.Filter = "XML-File | *.xml";
            if (f.ShowDialog() == DialogResult.OK)
            {
                statemandelbrot value = new statemandelbrot
                {

                    x = xstart,
                    y = ystart,
                    x1 = xende,
                    y1 = yende,
                    color = ColorPalate,
                };
                
                saveState(f.FileName,value);
            }
        }
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
              if (colorCycle == 4)
               {

                   colorCycle = 0;
                   HSBcol.colorCycling(colorCycle);
                   mandelbrot();
                   //start();
               }

               else
               {
                   colorCycle = colorCycle + 1;
                   HSBcol.colorCycling(colorCycle);
                   mandelbrot();
                   //start();

               }
            char[] colors = { 'R', 'G', 'B', 'P', 'D' };


            this.ColorPalate = colors[colorCycle];

            action = false; mouseDown = false; rectangle = false;
            
        }

        private void offToolStripMenuItem_Click(object sender, EventArgs e)
        {
            timer1.Stop();
        }

        private void Red_Click(object sender, EventArgs e)
        {
            ColorPalate = 'R';
            HSBcol.Color('R');
            mandelbrot();
        }

        private void Green_Click(object sender, EventArgs e)
        {
            ColorPalate = 'G';
            HSBcol.Color('G');
            mandelbrot();
        }

        private void Blue_Click(object sender, EventArgs e)
        {
            ColorPalate = 'B';
            HSBcol.Color('B');
            mandelbrot();
        }

        private void Pink_Click(object sender, EventArgs e)
        {
            ColorPalate = 'P';
            HSBcol.Color('P');
            mandelbrot();
        }

        private void Black_Click(object sender, EventArgs e)
        {
            ColorPalate = 'D';
            HSBcol.Color('D');
            mandelbrot();
        }

        private void ColorcycleON_Click(object sender, EventArgs e)
        {
            timer1.Interval = 1;
            timer1.Start();
        }

        private void ColorcycleOFF_Click(object sender, EventArgs e)
        {
            timer1.Stop();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

       

        private void loadToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            { 
                statemandelbrot value = new statemandelbrot();

                System.Xml.Serialization.XmlSerializer reader =
              new System.Xml.Serialization.XmlSerializer(typeof(statemandelbrot));
                //FileStream fsin = new FileStream("employee.binary", FileMode.Open, FileAccess.Read, FileShare.None);
                //string path = @"C:\files\Mandelbrot.xml";
                System.IO.FileStream file = System.IO.File.OpenRead(openFileDialog1.FileName);
                try
                {
                    using (file)
                    {
                        value = (statemandelbrot)reader.Deserialize(file);


                          newSX = value.x;
                          newSY = value.y;
                          newEX = value.x1;
                          newEY = value.y1;
                        newcolor = value.color;
                          mandelbrotStateValue = true;
                           initvalues();
                        HSBcol.Color(newcolor);
                        start();
                    }
                }
                catch
                {
                    MessageBox.Show("An error has occured");
                }
            }
        }

        private void restartToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            start();
        }

        public void Stop()
        {
            pictureBox1.Image = null;
            pictureBox1.Invalidate();
        }

        /*
         * Function to capture the co-ordinates of the mouse movement upward
        * when dragged inside the mandelbort canvas
        */
        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            //e.consume();
            mandelbrotStateValue = false;
            if (action)
            {
                mouseDown = true;
                xs = e.X;
                ys = e.Y;
            }
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            //e.consume();

            if (action && mouseDown)
            {
                xe = e.X;
                ye = e.Y;
                rectangle = true;
                pictureBox1.Refresh();
            }
        }

        /*
         * Function to capture the co-ordinates of the mouse movement upward
         * when dragged inside the mandelbort canvas
         */
        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            int z, w;

            //e.consume();
            if (action)
            {
                xe = e.X;
                ye = e.Y;
                if (xs > xe)
                {
                    z = xs;
                    xs = xe;
                    xe = z;
                }
                if (ys > ye)
                {
                    z = ys;
                    ys = ye;
                    ye = z;
                }
                w = (xe - xs);
                z = (ye - ys);
                if ((w < 2) && (z < 2))
                   
                initvalues();
                
                else
                {
                    if (((float)w > (float)z * xy)) ye = (int)((float)ys + (float)w / xy);
                    else xe = (int)((float)xs + (float)z * xy);
                    xende = xstart + xzoom * (double)xe;
                    yende = ystart + yzoom * (double)ye;
                    xstart += xzoom * (double)xs;
                    ystart += yzoom * (double)ys;
                    
                }
                xzoom = (xende - xstart) / (double)x1;
                yzoom = (yende - ystart) / (double)y1;
               // saveState(xstart, ystart, xende, yende,xzoom ,yzoom);
                mandelbrot();
                

                rectangle = false;
                pictureBox1.Refresh();
                mouseDown = false;
            }
        }


        public projectfractal()
        {
            InitializeComponent();
            HSBcol = new HSB();
            //setSize(640, 480);
            this.pictureBox1.Size = new System.Drawing.Size(640, 480); // equivalent of setSize in java code
            finished = false;
            c1 = Cursors.WaitCursor;
            c2 = Cursors.Cross;
            x1 = pictureBox1.Width;
            y1 = pictureBox1.Height;
            xy = (float)x1 / (float)y1;
            picture = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            g1 = Graphics.FromImage(picture);
            finished = true;

            start();

        }

        public void start()
        {
            action = false;
            rectangle = false;
            initvalues();
            xzoom = (xende - xstart) / (double)x1;
            yzoom = (yende - ystart) / (double)y1;

            mandelbrot();
            
        }

        /*
         * Reset the mandelbrot window and start values
         */
        private void initvalues()
        {
            if (mandelbrotStateValue)
            {
                xstart = newSX;
                ystart = newSY;
                xende = newEX;
                yende = newEY;
            }
            else
            {
                xstart = SX;
                ystart = SY;
                xende = EX;
                yende = EY;
            }
            if ((float)((xende - xstart) / (yende - ystart)) != xy)
                xstart = xende - (yende - ystart) * (double)xy;
            
        }

        /*
         * The main processing function for mandelbrot
         * The function will calulate all the mandelbrot point in our specified window
         * Each calculated point is then assigned a color based on 
         * hue, saturation and brightness and finally the point in drawn in
         * canvas i.e. pictureBox.
         * 
         */
        private void mandelbrot() 
        {
            int x, y;
            float h, b, alt = 0.0f;
            Pen pen = new Pen(Color.White);

            action = false;
            //this.Cursor = c1; // in java setCursor(c1)
            pictureBox1.Cursor = c2;

            //showStatus("Mandelbrot-Set will be produced - please wait..."); will do later
            for (x = 0; x < x1; x += 2)
            {
                for (y = 0; y < y1; y++)
                {
                   
                        h = pointcolour(xstart + xzoom * (double)x, ystart + yzoom * (double)y); // hue value

                        if (h != alt)
                        {
                            b = 1.0f - h * h; // brightness

                            HSBcol.fromHSB(h, 0.8f, b); //convert hsb to rgb then make a Java Color
                        // HSBcol.Color('G');
                        Color col = Color.FromArgb(Convert.ToByte(HSBcol.rChan), Convert.ToByte(HSBcol.gChan), Convert.ToByte(HSBcol.bChan));
                        
                            pen = new Pen(col);

                            //djm end
                            //djm added to convert to RGB from HSB

                            alt = h;

                     
                        //djm test
                       
                    }
                    
                    
                    
                   // g1.DrawLine(pen, new Point(x, y), new Point(x + 1, y)); // drawing pixel
                    g1.DrawLine(pen, new Point(x, y), new Point(x +1, y));
                   
                    
                }
                //showStatus("Mandelbrot-Set ready - please select zoom area with pressed mouse.");
                Cursor.Current = c1;
                action = true;
            }

            pictureBox1.Image = picture;
        }
       
        public void saveState(string filename , statemandelbrot value)
        {
           

            System.Xml.Serialization.XmlSerializer writer =
            new System.Xml.Serialization.XmlSerializer(typeof(statemandelbrot));

          
          
            System.IO.FileStream file = System.IO.File.Create(filename);
            
            

            try
            {
                using (file)
                {
                    writer.Serialize(file, value);
                 
                }
            }
            catch
            {
                MessageBox.Show("An error occured"); 

            }
            


        }

        private float pointcolour(double xwert, double ywert) // color value from 0.0 to 1.0 by iterations
        {
            double r = 0.0, i = 0.0, m = 0.0;// real, imaginary, absolute value or distance
            int j = 0;

            while ((j < MAX) && (m < 4.0))
            {
                j++;
                m = r * r - i * i; // x^2 - y^2
                i = 2.0 * r * i + ywert; // 2xy + c
                r = m + xwert;
            }
            return (float)j / (float)MAX;
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            update();
        }

        public void update()
        {
            Image tempImage = Image.FromHbitmap(picture.GetHbitmap());
            Graphics g = Graphics.FromImage(tempImage);

            if (rectangle)
            {
                Pen pen = new Pen(Color.White);

                Rectangle rect;

                if (xs < xe)
                {
                    if (ys < ye)
                    {
                        rect = new Rectangle(xs, ys, (xe - xs), (ye - ys));
                    }
                    else
                    {
                        rect = new Rectangle(xs, ye, (xe - xs), (ys - ye));
                    }
                }
                else
                {
                    if (ys < ye)
                    {
                        rect = new Rectangle(xe, ys, (xs - xe), (ye - ys));
                    }
                    else
                    {
                        rect = new Rectangle(xe, ye, (xs - xe), (ys - ye));
                    }
                }

                g.DrawRectangle(pen, rect);
                pictureBox1.Image = tempImage;

            }
        }
    }

}
